#include "ImagemCinza.h"

ImagemCinza *CriaImagemCinza(int xsize, int ysize)
{
  ImagemCinza *img=(ImagemCinza *)calloc(1,sizeof(ImagemCinza));

  img->xsize   = xsize; 
  img->ysize   = ysize; 
  img->npixels = xsize*ysize; 
  img->valor   = (int *)calloc(img->npixels,sizeof(int));
  
  return(img);
}

void         DestroiImagemCinza(ImagemCinza **img)
{
  ImagemCinza *aux=*img;
  
  free(aux->valor);
  free(aux);
  *img = NULL;
}

ImagemCinza *LeImagemP5(char *arq)
{
  FILE *fp=fopen(arq,"r");
  char  type[10];
  unsigned char *data;
  int   p, xsize, ysize, trash;
  ImagemCinza *img;
 
  if (fscanf(fp,"%s\n",type)!=1){
    printf("Erro de leitura\n");
    exit(-1);
  }

  if(strcmp(type,"P5")!=0) {
    printf("Formato invalido\n");
    exit(-1);
  }

  if (fscanf(fp,"%d %d\n",&xsize,&ysize)!=2){
    printf("Erro de leitura\n");
    exit(-1);
  }

  img = CriaImagemCinza(xsize,ysize);

  if (fscanf(fp,"%d\n",&trash)!=1){
    printf("Erro de leitura\n");
    exit(-1);
  }

  data  = (unsigned char *) calloc(img->npixels, sizeof(unsigned char));
  if (fread(data,sizeof(unsigned char),img->npixels,fp)!=img->npixels){
    printf("Erro de leitura\n");
    exit(-1);
  }

  for (p=0; p < img->npixels; p++){
    img->valor[p] = (int) data[p];
  }

  free(data);

  fclose(fp);

  return(img);
}

void         GravaImagemP5(ImagemCinza *img, char *arq)
{
  FILE *fp=fopen(arq,"w");
  char  type[10];
  unsigned char *data;
  int   p;
 
  fprintf(fp,"P5\n");
  fprintf(fp,"%d %d\n",img->xsize,img->ysize);
  fprintf(fp,"255\n");
  data  = (unsigned char *) calloc(img->npixels, sizeof(unsigned char));
  for (p=0; p < img->npixels; p++)
    data[p] = (unsigned char) img->valor[p];
  fwrite(data,sizeof(unsigned char),img->npixels,fp);
  free(data);

  fclose(fp);
}

int ValorMinimoImagemCinza(ImagemCinza *img)
{
  int p, min = INT_MAX;

  for (p=0; p < img->npixels; p++) 
    if (img->valor[p] < min)
      min = img->valor[p];

  return(min);
}

int ValorMaximoImagemCinza(ImagemCinza *img)
{
  int p, max = INT_MIN;

  for (p=0; p < img->npixels; p++) 
    if (img->valor[p] > max)
      max = img->valor[p];

  return(max);
}

ImagemCinza *EqualizaImagemCinza(ImagemCinza *img, int min, int max)
{
  FilaPrioridadesLinear *Q = CriaFilaPrioridadesLinear(ValorMinimoImagemCinza(img),ValorMaximoImagemCinza(img), img->npixels);
  ImagemCinza *eimg = CriaImagemCinza(img->xsize, img->ysize);
  int p, i, npixels_por_intervalo = img->npixels / (max - min + 1);

  for (p=0; p < img->npixels; p++){ 
    InsereFilaPrioridadesLinear(Q,p,img->valor[p]);
  }

  i = 0;
  while(!FilaPrioridadesLinearVazia(Q)){
    p = RemoveFilaPrioridadesLinear(Q);
    eimg->valor[p] = (i / npixels_por_intervalo) + min;
    i++;    
  }

  DestroiFilaPrioridadesLinear(&Q);
  return(eimg);
}
