#ifndef FILA_PRIORIDADE_LINEAR
#define FILA_PRIORIDADE_LINEAR

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <limits.h>

/* Esta é uma versao simples de fila de prioridades, onde as
   prioridades vao variar de um valor minimo a um valor maximo
   (inteiros) de um em um. Ou seja, max-min+1 sera o numero de
   listas. O total de nos dessas listas sera dado por nnos. Esta
   estrutura nao permite que nos com valores fora do intervalo
   [min,max] sejam armazenados. Os nos de uma mesma lista estao
   associados a um mesmo valor em [min,max] e eles entram e sai desta
   lista com politica first-in-first-out. Existem versoes mais
   complexas, que veremos ao final do curso. */

typedef struct nolistas {
  int prox, prev; 
} NoListas;

typedef struct filaprioridadeslinear {
  NoListas *no; /* arranjo de listas duplamente ligadas, uma para cada
		   valor de prioridade */
  int       nnos;    /* total de nos para armazenar todas as listas possiveis */
  int      *inicio;  /* apontadores para o inicio de todas as listas de
		        prioridades */
  int      *fim;     /* apontadores para o final de todas as listas de
		        prioridades */
  int       min, max; /* cada lista corresponde a um valor de prioridade que varia de min a max de 1 em 1 => nlistas = max-min+1 */
  int       nlistas;  /* numero de listas de prioridade */
  int       melhor_lista; /* indice da lista de maior prioridade. */
} FilaPrioridadesLinear;

FilaPrioridadesLinear *CriaFilaPrioridadesLinear(int min, int max, int nnos);
char                   FilaPrioridadesLinearVazia(FilaPrioridadesLinear *Q);
void                   ImprimeFilaPrioridadesLinear(FilaPrioridadesLinear *Q);
void                   AtualizaMelhorLista(FilaPrioridadesLinear *Q);
void                   DestroiFilaPrioridadesLinear(FilaPrioridadesLinear **Q);
void                   InsereFilaPrioridadesLinear(FilaPrioridadesLinear *Q, int no, int valor);
int                    RemoveFilaPrioridadesLinear(FilaPrioridadesLinear *Q);
  
#endif
