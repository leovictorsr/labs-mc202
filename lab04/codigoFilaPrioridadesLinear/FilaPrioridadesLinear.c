#include "FilaPrioridadesLinear.h"

FilaPrioridadesLinear *CriaFilaPrioridadesLinear(int min, int max, int nnos)
{
  FilaPrioridadesLinear *Q = (FilaPrioridadesLinear *)calloc(1,sizeof(FilaPrioridadesLinear));
  int i;

  if (min >= max) {
    printf("Valor minimo nao pode ser maior que o valor maximo (min=%d,max=%d)\n",min,max);
    exit(-1);
  }
  if (nnos <= 0) {
    printf("Numero de nos deve ser positivo (nnos=%d)\n",Q->nnos);
    exit(-1);
  }

  Q->nnos         =  nnos;
  Q->min          =  min;
  Q->max          =  max;
  Q->nlistas      =  max - min + 1;
  Q->melhor_lista =  Q->nlistas;
  Q->no           = (NoListas *)calloc(Q->nnos,sizeof(NoListas));
  Q->inicio       = (int *)calloc(Q->nlistas, sizeof(int));
  Q->fim          = (int *)calloc(Q->nlistas, sizeof(int));

  for (i=0; i < Q->nnos; i++) { /* inicializa todos os nos */ 
    Q->no[i].prev = Q->no[i].prox = -1;
  }

  for (i=0; i < Q->nlistas; i++) { /* inicializa todas as listas como vazias */ 
    Q->inicio[i] = Q->fim[i] = -1;
  }

  return(Q);
}

char FilaPrioridadesLinearVazia(FilaPrioridadesLinear *Q)
{

  AtualizaMelhorLista(Q);

  if (Q->melhor_lista == Q->nlistas) 
    return(1);
  else
    return(0);
}

void  DestroiFilaPrioridadesLinear(FilaPrioridadesLinear **Q)
{
  FilaPrioridadesLinear *aux = *Q;

  free(aux->no);
  free(aux->inicio);
  free(aux->fim);
  free(aux);
  *Q = NULL;
}

void  InsereFilaPrioridadesLinear(FilaPrioridadesLinear *Q, int no, int valor)
{
  int lista, ultimo, primeiro;

  if ((valor < Q->min)||(valor > Q->max)){
    printf("Valor %d a ser inserido deve estar em [%d,%d]\n",valor,Q->min,Q->max); 
    exit(-1);
  }

  if ((no < 0)||(no >= Q->nnos)){
    printf("No %d esta fora do intervalo [0,%d]\n",no,Q->nnos);
    exit(-1);
  }

  lista = valor - Q->min;

  if (lista < Q->melhor_lista) { /* atualiza a melhor lista */
    Q->melhor_lista = lista;
  }

  /* insere no final da lista */ 

  if (Q->fim[lista]==-1) { /* lista vazia */
    Q->fim[lista]    = no;
    Q->no[no].prox   = Q->no[no].prev = -1;
    Q->inicio[lista] = no;
  } else {
    primeiro             = Q->inicio[lista];
    ultimo               = Q->fim[lista];
    Q->fim[lista]        = no;
    Q->no[no].prox       = -1;
    Q->no[no].prev       = ultimo;
    Q->no[ultimo].prox   = no;
  }
}

void AtualizaMelhorLista(FilaPrioridadesLinear *Q)
{ 
  int i=0;

  while ((Q->inicio[i]==-1)&&(i<Q->nlistas))
    i++;
  
  Q->melhor_lista = i;

}

int   RemoveFilaPrioridadesLinear(FilaPrioridadesLinear *Q)
{
  int lista, no, primeiro, ultimo, proximo;

  lista = Q->melhor_lista;

  /* Remove o primeiro no da melhor lista */

  primeiro = Q->inicio[lista];
  ultimo   = Q->fim[lista];

  if (primeiro == ultimo) { /* lista ficara vazia */
    Q->inicio[lista]=Q->fim[lista]=-1;
  } else {
    proximo                          = Q->no[primeiro].prox;
    Q->inicio[lista]                 = proximo;
    Q->no[proximo].prev              = -1;
    if (proximo == ultimo){
      Q->no[proximo].prox = -1;
    }
  }
  
  no       = primeiro;
  Q->no[primeiro].prev=Q->no[primeiro].prox = -1;

  return(no);
}

void ImprimeFilaPrioridadesLinear(FilaPrioridadesLinear *Q)
{
  int i, p;
  
  for (i=0; i < Q->nlistas; i++) 
    if (Q->inicio[i]!=-1) {
      printf("inicio %d fim %d valor %d\n",Q->inicio[i],Q->fim[i],i+Q->min);
      p = Q->inicio[i];
      do { 
	printf("(prev %d) %d (prox %d)", Q->no[p].prev, p, Q->no[p].prox);
	p = Q->no[p].prox;
      } while (p != -1);
      printf("\n");
    }
}

