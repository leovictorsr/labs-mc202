#include "FilaPrioridadesLinear.h"
#include "FilaPrioridadesLinear.c"

int main()
{
  FilaPrioridadesLinear *Q = CriaFilaPrioridadesLinear(-100,100,10);
  int no;

  /*----------------------------------------------------------------------*/

  void *trash = malloc(1);                 
  struct mallinfo info;   
  int MemDinInicial, MemDinFinal;
  free(trash); 
  info = mallinfo();
  MemDinInicial = info.uordblks;

  /*----------------------------------------------------------------------*/



  if (FilaPrioridadesLinearVazia(Q)) 
    printf("Fila criada esta vazia\n");

  
  InsereFilaPrioridadesLinear(Q,5,30);
  InsereFilaPrioridadesLinear(Q,4,30);
  InsereFilaPrioridadesLinear(Q,1,-10);
  InsereFilaPrioridadesLinear(Q,9,-10);
  InsereFilaPrioridadesLinear(Q,3,1);
  InsereFilaPrioridadesLinear(Q,8,30);

  ImprimeFilaPrioridadesLinear(Q);

  while (!FilaPrioridadesLinearVazia(Q)){
    no = RemoveFilaPrioridadesLinear(Q);
    printf("no %d removido\n",no);
    ImprimeFilaPrioridadesLinear(Q);
  }

  /* -------------------------------------------------------------------- */

  info = mallinfo();
  MemDinFinal = info.uordblks;
  if (MemDinInicial!=MemDinFinal)
    printf("\n\nDinamic memory was not completely deallocated (%d, %d)\n",
	   MemDinInicial,MemDinFinal);   

  /* -------------------------------------------------------------------- */


  return(0);

}
