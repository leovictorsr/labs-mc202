#include "ImagemCinza.h"
#include "FilaPrioridadesLinear.c"
#include "ImagemCinza.c"


int main(int argc, char *argv[])
{
  ImagemCinza *img, *eimg;


  if (argc != 3){
    printf("EqualizaImagemCinza <imagem.pgm> <imagem_equalizada.pgm>\n");
    exit(-1);
  }

  /*----------------------------------------------------------------------*/

  void *trash = malloc(1);                 
  struct mallinfo info;   
  int MemDinInicial, MemDinFinal;
  free(trash); 
  info = mallinfo();
  MemDinInicial = info.uordblks;

  /*----------------------------------------------------------------------*/

  
  img  = LeImagemP5(argv[1]);
  eimg = EqualizaImagemCinza(img,0,255);
  GravaImagemP5(eimg, argv[2]);

  DestroiImagemCinza(&img);
  DestroiImagemCinza(&eimg);

  /* -------------------------------------------------------------------- */

  info = mallinfo();
  MemDinFinal = info.uordblks;
  if (MemDinInicial!=MemDinFinal)
    printf("\n\nDinamic memory was not completely deallocated (%d, %d)\n",
	   MemDinInicial,MemDinFinal);   

  /* -------------------------------------------------------------------- */




  return(0);
}
