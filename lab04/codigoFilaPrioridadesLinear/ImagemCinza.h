#ifndef IMAGEMCINZA
#define IMAGEMCINZA

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <limits.h>
#include "FilaPrioridadesLinear.h"


typedef struct imagem_cinza {
  int *valor;
  int  npixels, xsize, ysize; /* npixels = xsize*ysize */ 
} ImagemCinza;

ImagemCinza *CriaImagemCinza(int xsize, int ysize);
void         DestroiImagemCinza(ImagemCinza **img);
ImagemCinza *LeImagemP5(char *arq);
void         GravaImagemP5(ImagemCinza *img, char *arq);
int          ValorMinimoImagemCinza(ImagemCinza *img); 
int          ValorMaximoImagemCinza(ImagemCinza *img); 
ImagemCinza *EqualizaImagemCinza(ImagemCinza *img, int min, int max); 
  
#endif
