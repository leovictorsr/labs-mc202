#ifndef SEQUENCIA
#define SEQUENCIA

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <limits.h>

typedef struct sequencia {
  int *valor;
  int  nelems;
} Sequencia;

Sequencia *LeSequencia(char *arq);
void       DestroiSequencia(Sequencia **S);
int        ValorMaximo(Sequencia *S); 
int        ValorMinimo(Sequencia *S); 
Sequencia *OrdenaSequencia(Sequencia *A);
void       ImprimeSequencia(Sequencia *S);


#endif
