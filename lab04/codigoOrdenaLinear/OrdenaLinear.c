#include "Sequencia.h"
#include "Sequencia.c"


int main(int argc, char *argv[])
{
  Sequencia *A,*B;

  if (argc != 2) {
    printf("OrdenaLinear <sequencia.txt>\n");
    exit(-1);
  }

  A = LeSequencia(argv[1]);
  ImprimeSequencia(A);
  B = OrdenaSequencia(A);
  ImprimeSequencia(B);
  DestroiSequencia(&A);
  DestroiSequencia(&B);

  return(0);
}
