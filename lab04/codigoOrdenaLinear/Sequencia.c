#include "Sequencia.h"

Sequencia *CriaSequencia(int nelems)
{
  Sequencia *S=(Sequencia *)calloc(1,sizeof(Sequencia));
 
  S->valor  = (int *)calloc(nelems,sizeof(int));
  S->nelems = nelems;
  
  return(S);
}

Sequencia *LeSequencia(char *arq)
{
  FILE *fp = fopen(arq,"r");
  int   i, nelems;
  Sequencia *S=NULL;

  fscanf(fp,"%d\n",&nelems);
  S = CriaSequencia(nelems);
  for (i=0; i < S->nelems; i++) 
    fscanf(fp,"%d",&S->valor[i]);

  fclose(fp);

  return(S);
}

void       DestroiSequencia(Sequencia **S)
{
  free((*S)->valor);
  free(*S);
}

int        ValorMaximo(Sequencia *S)
{
  int i, Vmax=INT_MIN;

  for (i=0; i < S->nelems; i++) 
    if (S->valor[i]>Vmax) 
      Vmax = S->valor[i];

  return(Vmax);
}

int        ValorMinimo(Sequencia *S)
{
  int i, Vmin=INT_MAX;

  for (i=0; i < S->nelems; i++) 
    if (S->valor[i]<Vmin) 
      Vmin = S->valor[i];

  return(Vmin);
}

Sequencia *OrdenaSequencia(Sequencia *A)
{
  Sequencia *B = CriaSequencia(A->nelems); 
  int Vmin     = ValorMinimo(A);
  int Vmax     = ValorMaximo(A);
  int i,j,v,*Q = (int *)calloc(Vmax-Vmin+1,sizeof(int));

  for (i=0; i < A->nelems; i++) 
    Q[A->valor[i]-Vmin]++;

  j = 0; 
  for (v=0; v <= Vmax-Vmin; v++) {
    for (i=0; i < Q[v]; i++, j++) {
      B->valor[j] = v+Vmin;
    }
  }

  return(B);
}

void ImprimeSequencia(Sequencia *S)
{
  int i;

  for (i=0; i < S->nelems; i++) 
    printf("%d ",S->valor[i]);
  
  printf("\n");
}
