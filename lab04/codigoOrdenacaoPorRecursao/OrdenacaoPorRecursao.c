#include <stdio.h>
#include <malloc.h>

void Switch(int *v1, int *v2)
{
    int aux = *v1;

    *v1 = *v2; *v2 = aux;
}

void BubbleSort(int *A, int n)
{
    int i;

    if (n > 1){
       for (i=0; i < n-1; i++)
         if (A[i] > A[i+1])
            Switch(&A[i],&A[i+1]);
       BubbleSort(A,n-1);
    }
}

void InsertionSort(int *A, int n)
{
    int i;

    if (n > 1){
       InsertionSort(A,n-1);
       i = n-1;
       while ((i>0)&&(A[i]<A[i-1])) {
        Switch(&A[i],&A[i-1]);
        i--;
       }
    }
}

void SelectionSort(int *A, int n)
{
   int i, imax=0;
   if (n > 1){
    for (i=1; i < n; i++)
        if (A[i] > A[imax])
            imax = i;
    Switch(&A[imax],&A[n-1]);
    SelectionSort(A,n-1);
   }
}

void Imprime(int *A, int n)
{
    int i;

    for (i=0; i < n; i++)
        printf("%d ",A[i]);

    printf("\n");
}

void Merge(int *A, int p, int r, int q)
{
    int i=p, j=r+1, k=0, *B=(int *)calloc(q-p+1,sizeof(int));

    while ((i<=r)&&(j<=q)){
      if (A[i] <= A[j]){
        B[k] = A[i]; k++; i++;
      } else {
        B[k] = A[j]; k++; j++;
      }
    }
    while (i<=r){
      B[k] = A[i]; k++; i++;
    }
    while (j<=q){
      B[k] = A[j]; k++; j++;
    }
    for (k=0, i=p; i <= q; i++, k++)
       A[i] = B[k];

    Imprime(B,q-p+1);
    free(B);

}

void MergeSort(int *A, int p, int q)
{
  int r;

    if (p < q) {
      r = (p + q)/2; /* divisão */
      MergeSort(A,p,r);
      MergeSort(A,r+1,q);
      Merge(A,p,r,q);
    }
}

int Pivot(int *A, int p, int q)
{
  int x = A[p], i = p, j = q;
  
  do {
    while (A[i] <= x) i++;
    while (A[j]  > x) j--;
    if (i < j) {
      Switch(&A[i],&A[j]);
      i++; j--; 
    }
  } while (i < j);
  Switch(&A[p],&A[j]);
  return(j);
}

void QuickSort(int *A, int p, int q)
{
  int r;

  if (p < q) {
    r = Pivot(A,p,q);
    QuickSort(A,p,r-1);
    QuickSort(A,r+1,q);
  }

}




int main()
{
    int A[8]={20,0,2,7,2,20,4,0};

    QuickSort(A,0,7);
    Imprime(A,8);

 return(0);
}
