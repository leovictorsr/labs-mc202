/**
 * Nome: Leonardo Victor da Silva Rodrigues
 * RA: 172109
 * Laboratório 04 - Os Caça-Pixels
 */

/* Bibliotecas */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Constantes */

/* Structs */
typedef struct Bucket {
    struct Bucket* next;
    struct Bucket* previous;
    int size;
    int* array;
} Bucket;

typedef struct Row {
    int minimum, maximum, length, size;
    struct Bucket** array;
    int* ordered;
    int head;
    int tail;
} Row;

typedef struct Image{
    int n;
    int* pixel;
} Image;

/* Protótipo de funções */
Row* bucketize (Image* image);
Row* createRow (int min, int max, int size);
Bucket* createBucket (int size);
void destroyImage(Image** image);
void getRange (Image* image, int* range);
void insert (int position, Bucket* bucket);
void printIntervals (Row* row, int n, int* range);
void prune (Row* row);
Image *readImageP5(const char *format);
void readPixels (Image* image, Row* row);
void skipComments(FILE *fp);

/**
 * Eu sou a função principal.
 * Eu gerencio as operações feitas sobre a imagem.
 *
 * :retorna: 0
 * :tipo de retorno: int
 */
int main ()
{
    /* Declaração de variáveis */
    char imageName[11];
    int i, nIntervals;
    int* range = (int*) calloc (2, sizeof(int));;
    Image* image;

    /* Pego o nome do arquivo de imagem */
    fgets(imageName, sizeof(imageName), stdin);

    /* Pego o arquivo de imagem */
    image = readImageP5(imageName);

    /* Pego o valor do número de intervalos */
    scanf("%d\n", &nIntervals);

    /* Pego o itervalo desejado */
    scanf("%d %d\n", &range[0], &range[1]);

    /* Ordena a imagem */
    Row* oImage = bucketize(image);

    /* Imprimo os pixels no intervalo */
    printIntervals(oImage, nIntervals, range);

    return 0;
}


/**
 * Eu ordeno os pixels da imagem por brilho.
 *
 * :param image: a imagem a ter os pixels ordenados
 * :type  image: Image*
 *
 * :param nBuckets: o número de 'buckets' a ser usado
 * :type  nBuckets: int
 *
 * :retorna: a fila construída e ordenada
 * :tipo de retorno: Row*
 */
Row* bucketize (Image* image)
{
    /* Pego os limites de valores da imagem */
    int* range = (int*) calloc (2, sizeof(int));
    getRange(image, range);

    /* Crio a fila com os buckets */
    Row* row = createRow(range[0], range[1], image->n);

    /* Leio os pixels da imagem inserindo na fila de buckets */
    readPixels(image, row);

    /* Faço uma lista com os buckets válidos */
    prune(row);

    return row;
}


/**
 * Eu crio uma fila de prioridades.
 *
 * :param min: o valor mínimo encontrado na fila
 * :type  min: int
 *
 * :param max: o valor máximo encontrado na fila
 * :type  max: INT
 *
 * :param size: o número de nós presentes na fila
 * :type  size: int
 *
 * :retorna: a fila criada
 * :tipo de retorno: Row*
 */
Row* createRow (int min, int max, int size)
{
    /* Declaração de variáveis locais */
    int i;

    /* Pego o número de buckets */
    int length = (max - min) + 1;

    /* Crio a fila */
    Row* row = (Row*) calloc (1, sizeof(Row));
    row->size = size;
    row->length = length;
    row->minimum = min;
    row->maximum = max;
    row->array = (Bucket**) calloc (length, sizeof(Bucket));
    row->ordered = (int *) calloc (size, sizeof(int));
    row->head = -1;
    row->tail = -1;

    /* Inicializo o vetor de buckets e os buckets */
    for (i = 0; i < length; i++)
    {
        row->array[i] = createBucket(size);
    }

    /* Retorno a fila */
    return row;
}


/**
 * Eu crio um bucket que armazena inteiros.
 *
 * :param size: o tamanho do bucket
 * :type  size: int
 *
 * :retorna: o bucket criado
 * :tipo de retorno: Bucket*
 */
Bucket* createBucket (int size)
{
    /* Declaração de variáveis locais */
    int i;

    /* Aloco a memória para o bucket de inteiros */
    Bucket* bucket = (Bucket*) calloc (1, sizeof(Bucket));

    /* Aloco a memória para o vetor de inteiros do bucket */
    bucket->array = (int*) calloc (size, sizeof(int));
    bucket->size = size;

    /* Inicializo o vetor de inteiros do bucket */
    for (i = 0; i < size; i++)
    {
        bucket->array[i] = -1;
    }

    return bucket;
}


/**
 * Eu pego o maior e o menor valor de brilho de uma imagem.
 *
 * :param image: a imagem a ser pego o valor máximo
 * :type  image: Image*
 *
 * :retorna: os valores mínimo e máximo de brilho na imagem
 * :tipo de retorno: int*
 */
void getRange (Image* image, int* range)
{
    /* Declaração de variáveis locais */
    int i;

    /* Inicializo os parâmetros */
    range[0] = image->pixel[0];
    range[1] = image->pixel[0];

    /* Percorro a imagem */
    for (i = 0; i < image->n; i++)
    {
        /* Brilho é menor que o armazenado: atribui novo valor mínimo */
        if (image->pixel[i] < range[0])
            range[0] = image->pixel[i];

        /* Brilho é menor que o armazenado: atribui novo valor máximo */
        else if (image->pixel[i] > range[1])
            range[1] = image->pixel[i];
    }
}


/**
 * Eu insiro um pixel em um bucket.
 *
 * :param value: o valor do pixel a ser inserido
 * :type  value: int
 *
 * :param position: a posição do pixel a ser inserido
 * :type  position: int
 *
 * :param bucket: o bucket a ser inserido o pixel
 * :type  bucket: Bucket*
 */
void insert (int position, Bucket* bucket)
{
    /* Declaração de variáveis locais */
    int i;
    
    /* Percorro o bucket até a primeira posição onde possa inserir */
    for (i = 0; bucket->array[i] != -1 && i < bucket->size; i++);
    bucket->array[i] = position;
}


/**
 * Eu imprimo as posições dos pixels nos intervalos desejados.
 *
 * :param row: a fila com os pixels
 * :type  row: Row*
 *
 * :param n: o numero de intervalos
 * :type  n: int
 *
 * :param range: os limites dos intervalos a serem impressos
 * :type  range: int*
 *
 * :retorna: nada
 * :tipo de retorno: void
 */

void printIntervals (Row* row, int n, int* range)
{
    /* Declaração de variáveis locais */
    int i, j, diff;
    Bucket* head = row->array[row->head];
    int* elements = (int *) calloc (row->size, sizeof(int));
    diff = (row->size / (n + 1));

    for (i = 0; i < row->size;)
    {
        for (j = 0; head->array[j] != -1; j++, i++)
        {
            row->ordered[i] = head->array[j];
        }
        head = head->next;
    }

    i = n * range[0];
    j = n * range[1] + row->size/n - 1;

    while (i <= j)
    {
        printf("%d ", row->ordered[i]);
        i++;
    }

    printf("\n");
}


/**
 * Eu analiso os buckets na fila e faço uma lista duplamente ligada com os
 * buckets que possuem valores armazenados.
 *
 * :param row: a fila a ser analisada
 * :type  row: Row*
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void prune (Row* row)
{
    /* Declaração de variáveis locais */
    int i, j;

    /* Percorro a fila */
    for (i = 0; i < row->length; i++)
    {
        /* Bucket tem valor armazenado: insiro ele na lista de buckets */
        if (row->array[i]->array[0] != -1)
        {
            /* Lista de buckets não iniciada: a inicia */
            if (row->head == -1)
            {
                row->head = i;
                row->tail = i;
            }

            /* Lista de buckets já iniciada: insere no fim */
            else
            {   
                row->array[i]->previous = row->array[row->tail];
                row->array[row->tail]->next = row->array[i];
                row->tail = i;
            }
        }
    }
}


/**
 * Eu leio o valor de brilho dos pixels e armazeno cada pixel da imagem num
 * bucket da array de buckets da fila.
 *
 * :param image: a imagem origem dos pixel
 * :type  image: Image*
 *
 * :param row: a fila a ser preenchida
 * :type  row: Row*
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void readPixels (Image* image, Row* row)
{
    /* Declaração de variáveis locais */
    int i;

    /* Percorro a imagem */
    for (i = 0; i < image->n; i++)
    {
        insert(i, row->array[image->pixel[i]]);
    }
}

void skipComments(FILE *fp)
{
    //skip for comments
    while (fgetc(fp) == '#') {
        while (fgetc(fp) != '\n');
    }
    fseek(fp,-1,SEEK_CUR);
}

Image *readImageP5(const char *filename)
{
    int *img = NULL;
    FILE    *fp=NULL;
    unsigned char   *data8=NULL;
    char    type[10];
    int     n, p,xsize,ysize,max;

    fp = fopen(filename,"rb");
    if (fp == NULL){
        printf("Error opening image.\n");
        exit(1);
    }

    if (fscanf(fp,"%s\n",type) != 1){
        printf("Reading error\n");
        exit(1);
    }

    if((strcmp(type,"P5")==0)) {
        skipComments(fp);
        if (fscanf(fp, "%d %d\n", &xsize, &ysize) != 2) {
            printf("Reading error\n");
            exit(1);
        }

        if (fscanf(fp, "%d\n", &max) != 1) {
            printf("Reading error\n");
            exit(1);
        }

        n = xsize*ysize;
        data8 = calloc(n, sizeof(unsigned char));
        if (fread(data8,sizeof(unsigned char),n,fp)!=n) {
            printf("Reading error\n");
            exit(1);
        }

        img = calloc(n, sizeof(int));
        for (p = 0; p < n; ++p) {
            img[p] = (int)data8[p];
        }
    }

    free(data8);
    fclose(fp);

    Image* image = (Image*)calloc(1, sizeof(Image));
    image->n = n;
    image->pixel = img;

    return(image);
}

void destroyImage(Image** image) {
    free((*image)->pixel);
    free(*image);
    *image = NULL;
}
