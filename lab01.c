/**
 * Nome: Leonardo Victor da Silva Rodrigues
 * RA: 172109
 * Laboratorio 01 - O problema do acesso a lista
 */

/* Bibliotecas */
#include <stdio.h>
#include <stdlib.h>

/* Constantes */
#define SUCCESS 1
#define FAILURE 0

 /* Structs */
 /* Struct para Nó */
 typedef struct Node {
    int el;
    struct Node* previous;
    struct Node* next;
 } Node;

/* Struct para Lista */
 typedef struct List{
    struct Node* head;
 } List;


/* Prototipo de funcoes */
int accessNodeMtf (List* list, int key);
int accessNodeTranspose (List* list, int key);
List* createList ();
Node* createNode ();
void freeList(List* list);
void insertNodeDefault (List* list, int key);
int insertNodeMtf (List* list, int key);
int insertNodeTranspose (List* list, int key);
int isListEmpty (List* list);
void printList (List* list);
int removeNode (List* list, int key);


/** 
 * Eu sou a função principal do programa.
 * Eu chamo as funções que gerenciam a lista.
 */
int main ()
{
    /* Declaração de variáveis locais */
    int tam, requests, key, i, trCost, mtfCost;
    char op;
    Node* node;
    List* trList;
    List* mtfList;

    /* Inicialização de variáveis locais */
    trCost = 0;
    mtfCost = 0;

    /* Le o numero de elementos e de requisicoes */
    scanf("%d %d", &tam, &requests);

    /* Cria as listas */
    trList = createList();
    mtfList = createList();

    /* Cria os nós da lista */
    for (i = 0; i < tam; i++)
    {
        scanf("%d", &key);
        insertNodeDefault(mtfList, key);
        insertNodeDefault(trList, key);
    }

    /* Leio as operações e as executo */
    scanf(" ");
    for (i = 0; i < requests; i++)
    {
        scanf("%c %d", &op, &key);
        scanf(" ");
        switch (op)
        {
            /* a: acesso o nó com elemento igual a key */
            case 'a':
                mtfCost += accessNodeMtf(mtfList, key);
                trCost += accessNodeTranspose(trList, key);
                break;

            /* r: removo o nó com elemento igual a key */
            case 'r':
                mtfCost += removeNode(mtfList, key);
                trCost += removeNode(trList, key);
                break;

            /* i: insiro o nó com elemento igual a key */
            case 'i':
                mtfCost += insertNodeMtf(mtfList, key);
                trCost += insertNodeTranspose(trList, key);
                break; 
        }
    }

    /* Imprimo a lista onde foi aplicado o algoritmo MTF e o custo */
    printf("%d\n", mtfCost);
    printList(mtfList);

    /* Imprimo a lista onde foi aplicado o algoritmo Tranpose e o custo */
    printf("%d\n", trCost);
    printList(trList);

    /* Libera a memória das listas */
    freeList(mtfList);
    freeList(trList);
    return 0;
}

/**
 * Eu acesso um nó usando o algoritmo 'move-to-front'.
 *
 * :param list: a lista a ser percorrida
 * :type  list: List*
 *
 * :param key: a chave do nó a ser acessado
 * :type  key: int
 *
 * :retorna: o custo da operação
 * :tipo de retorno: int
 */
int accessNodeMtf (List* list, int key)
{
    /* Declaração de variáveis locais */
    int cost = 0;

    /* Posiciono o nó posição no nó cabeça */
    Node* position = list->head;

    /* Percorro a lista procurando pelo nó cujo elemento seja igual a key */
    cost++;
    while (position != NULL && position->el != key)
    {
        position = position->next;

        /* Incremento o custo */
        cost++;
    }

    /* Cheguei ao fim da lista: retorno FAILURE */
    if (position == NULL)
        return FAILURE;

    /* Nó encontrado não é o cabeça: reposiciono seu vizinho da esquerda */
    if (position->previous != NULL)
    {       
        position->previous->next = position->next;
    }

    /* Nó encontrado é o cabeça: retorna o custo */
    else
        return cost;
    
    /* Nó encontrado não é o último: reposiciono seu vizinho da direita */
    if (position->next != NULL)
    {
        position->next->previous = position->previous;      
    }

    /* Faz o nó encontrado ser o cabeça */
    position->previous = NULL;
    position->next = list->head;
    position->next->previous = position;
    list->head = position;

    /* Retorno o custo */
    return cost;
}

/**
 * Eu acesso um nó usando o algoritmo 'transpose'.
 *
 * :param list: a lista a ser percorrida
 * :type  list: List*
 *
 * :param key: a chave do nó a ser acessado
 * :type  key: int
 *
 * :retorna: o custo da operação
 * :tipo de retorno: int
 */
int accessNodeTranspose (List* list, int key)
{
    /* Declaração de variáveis locais */
    int cost = 0;

    /* Posiciono o nó posição no nó cabeça */
    Node* position = list->head;

    /* Percorro a lista procurando pelo nó cujo elemento seja igual a key */
    cost++;
    while (position != NULL && position->el != key)
    {
        position = position->next;
        
        /* Incremento o custo */
        cost++;
    }

    /* Cheguei ao fim da lista: retorno FAILURE */
    if (position == NULL)
        return FAILURE;

    /* Nó encontrado não é o cabeça: reposiciono seu vizinho da esquerda */
    if (position->previous != NULL)
    {       
        position->previous->next = position->next;
    }

    /* Nó encontrado é o cabeça: retorna o custo */
    else
        return cost;
    
    /* Nó encontrado não é o último: reposiciono seu vizinho da direita */
    if (position->next != NULL)
    {
        position->next->previous = position->previous;      
    }

    /* Nó anterior ao anterior é nulo: nó encontrado se torna cabeça */
    if (position->previous->previous == NULL)
    {
        position->next = position->previous;
        position->next->previous = position;
        position->previous = NULL;
        list->head = position;
    }

    /* Se não: troca o nó com seu anterior */
    else
    {
        position->previous->previous->next = position;
        position->next = position->previous;
        position->previous = position->previous->previous;
        position->next->previous = position;
    }

    /* Retorno o custo */
    return cost;
}

/**
 * Eu crio uma lista.
 *
 * :retorna: a lista criada
 * :tipo de retorno: List*
 */
List* createList()
{
    return (List *) calloc(1, sizeof(List));
}

/**
 * Eu crio um nó.
 *
 * :retorna: o nó criado
 * :tipo de retorno: Node*
 */
Node* createNode()
{
    return (Node *) calloc(1, sizeof(Node));
}

/**
 * Eu libero a memória de uma lista.
 *
 * :param list: a lista a ser liberada memória
 * :type  list: List*
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void freeList (List* list)
{
    /* Lista vazia: nada a fazer */
    if (isListEmpty(list))
        return;
    
    Node* position = list->head->next;

    /* Percorre a lista liberando a memória dos nós */
    while(position->next != NULL)
    {
        free(position->previous);
        position = position->next;
    }
    free(position);

    /* Libera a memória ocupada pela própria lista */
    free(list);
}

/**
 * Eu insiro um nó no final da lista.
 *
 * :param list: a lista onde será inserido o nó
 * :type  list: List*
 *
 * :param key: a chave a ser armazenada no nó
 * :type  key: int
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void insertNodeDefault (List* list, int key)
{
    /* Crio o nó e armazeno a chave */
    Node* node = createNode();
    node->el = key;

    /* Lista é vazia: insere o nó e retorna */
    if (isListEmpty(list))
    {
        list->head = node;
        return;
    }

    /* Pego o nó cabeça da lista */
    Node *position = list->head;

    /* Alcanço o final da lista */
    while (position->next != NULL)
    {
        position = position->next;
    }

    /* Posiciono o nó criado na lista */
    node->previous = position;
    position->next = node;
}

/**
 * Eu insiro um nó na lista usando o algoritmo 'move-to-front'.
 *
 * :param list: a lista a ser adicionado o nó
 * :type  list: List*
 *
 * :param key: a chave do nó a ser adicionado
 * :type  key: int
 *
 * :retorna: o custo da operação
 * :tipo de retorno: int
 */
int insertNodeMtf (List* list, int key)
{
    /* Declaração de variáveis locais */
    int cost = 0;

    /* Crio o nó e armazeno a chave */
    Node* node = createNode();
    node->el = key;

    /* Pego o nó cabeça da lista */
    Node *position = list->head;

    /* Alcanço o final da lista */
    cost++;
    while (position->next != NULL)
    {
        cost++;
        position = position->next;
    }

    /* Posiciono o nó criado na lista */
    cost++;
    node->previous = position;
    position->next = node;

    /* Retorno o custo */
    return cost;
}

/**
 * Eu insiro um nó na lista usando o algoritmo 'transpose'.
 *
 * :param list: a lista a ser adicionado o nó
 * :type  list: List*
 *
 * :param key: a chave do nó a ser adicionado
 * :type  key: int
 *
 * :retorna: o custo da operação
 * :tipo de retorno: int
 */
int insertNodeTranspose (List* list, int key)
{
    /* Declaração de variáveis locais */
    int cost = 0;

    /* Crio o nó e armazeno a chave */
    Node* node = createNode();
    node->el = key;

    /* Pego o nó cabeça da lista */
    Node *position = list->head;

    /* Alcanço o final da lista */
    cost++;
    while (position->next != NULL)
    {
        cost++;
        position = position->next;
    }

    /* Posiciono o nó criado na lista */
    cost++;
    node->next = position;

    /* Lista só tem nó cabeça: nó criado entra como cabeça */
    if (position->previous == NULL)
    {
        node->previous = NULL;
        list->head = node;
    }

    /* Se não: nó criado troca de posição com o último */
    else
    {
        node->previous = position->previous;
        position->previous->next = node;
    }
    position->previous = node;

    /* Retorno o custo */
    return cost;
}

/**
 * Eu checo se uma lista é vazia.
 *
 * :param list: a lista a ser checada
 * :type  list: List*
 *
 * :retorna: SUCCESS ou FAILURE
 * :tipo de retorno: int
 */
int isListEmpty(List* list)
{
    /* Lista é nula: retorna sucesso */
    if (list == NULL)
    {
        return SUCCESS;        
    }

    /* Nó cabeça é nulo: retorna sucesso */
    else if (list->head == NULL)
    {
        return SUCCESS;
    }

    /* Retorna falha */
    return FAILURE;
}
/**
 * Eu imprimo uma lista.
 *
 * :param list: a lista a ser impressa
 * :type  list: List*
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void printList (List* list)
{
    /* Pego o nó cabeça */
    Node* node = list->head;

    /* Percorro a linha imprimindo os elementos */
    while (node != NULL)
    {
        printf("%d ", node->el);
        node = node->next;
    }
    printf("\n");
}

/**
 * Eu removo um nó da lista usando o algoritmo 'move-to-front'.
 *
 * :param list: a lista a ser removido o nó
 * :type  list: List*
 *
 * :param key: a chave do nó a ser removido
 * :type  key: int
 *
 * :retorna: o custo da operação
 * :tipo de retorno: int
 */
int removeNode (List* list, int key)
{
    /* Declaração de variáveis locais */
    int cost = 0;

    /* Posiciono o nó posição no nó cabeça */
    Node* position = list->head;

    /* Percorro a lista procurando pelo nó cujo elemento seja igual a key */
    cost++;
    while (position != NULL && position->el != key)
    {
        /* Incremento o custo */
        cost++;

        /* Percorro a lista */
        position = position->next;
    }

    /* Cheguei ao fim da lista: retorno FAILURE */
    if (position == NULL)
        return FAILURE;

    /* Nó encontrado não é o cabeça: reposiciono seu vizinho da esquerda */
    if (position->previous != NULL)
    {       
        position->previous->next = position->next;
    }

    /* Se nó é o cabeça: reposiciono o nó cabeça da lista */
    else
        list->head = position->next;
    
    /* Nó encontrado não é o último: reposiciono seu vizinho da direita */
    if (position->next != NULL)
    {
        position->next->previous = position->previous;      
    }


    free(position);

    /* Retorno o custo */
    return cost;
}