/**
 * Nome: Leonardo Victor da Silva Rodrigues
 * RA: 172109
 * Laboratório 03: Sistema Único de Saúde
 */

// bibliotecas
#include <stdio.h>
#include <stdlib.h>

// constantes
#define SUCCESS 1
#define FAILURE 0

// struct para nós
typedef struct Node {
    int key;
    struct Node* next;
    struct Node* previous;
} Node;

// struct para filas
typedef struct Fifo {
    struct Node* head;
    struct Node* tail;
} Fifo;

// protótipo das funções
int attend (Fifo* fifo);
Fifo* createFifo ();
Node* createNode ();
void enqueue (Fifo* fifo, int key);
void freeFifo (Fifo* fifo);
int isFifoEmpty (Fifo* fifo);
void prioritize (Fifo* fifo, int key);

/**
 * Eu sou a função principal.
 * Eu gerencio as operações da fila.
 *
 * :retorna: 0
 * :tipo de retorno: int
 */
int main ()
{
    // variáveis locais
    int key, i, n, flag;
    char operation;

    // crio a fila
    Fifo* fifo = createFifo();

    // leio o número de elementos
    scanf("%d", &n);
    scanf(" ");

    // insiro-os na fila
    for (i = 1; i <= n; i++)
        enqueue(fifo, i);

    // inicio as operações
    flag = SUCCESS;
    while (flag)
    {
        // leio a operação
        scanf("%c", &operation);
        scanf(" ");
        switch (operation)
        {
            // atendimento
            case 'A':
                printf("%d\n", attend(fifo));
                break;

            // priorização
            case 'E':
                scanf("%d", &key);
                scanf(" ");
                prioritize(fifo, key);
                break;

            // fim de operações
            case 'F':
                flag = FAILURE;
                break;
        }
    }

    // libero a memória da fila
    freeFifo(fifo);
    return 0;
}

/**
 * Eu atendo a fila na ordem.
 *
 * :param fifo: a fila a ser atendida
 * :type  fifo: Fifo*
 *
 * :retorna: a chave do elemento atendido
 * :tipo de retorno: void
 */
int attend (Fifo* fifo)
{
    // fila vazia: retorna FAILURE
    if (isFifoEmpty(fifo))
        return FAILURE;

    // pego o nó cabeça
    Node* head = fifo->head;

    // transfiro o nó cabeça
    fifo->head = head->next;
    fifo->tail = head;

    // retorna a chave do nó atendido
    return head->key;
}

/**
 * Eu crio uma fila
 *
 * :retorna: a fila criada
 * :tipo de retorno: Fifo*
 */
Fifo* createFifo ()
{
    return (Fifo *) calloc (1, sizeof(Fifo));
}

/**
 * Eu crio um nó.
 *
 * :retorna: o nó criado
 * :tipo de retorno: Node*
 */
Node* createNode ()
{
    return (Node *) calloc (1, sizeof(Node));
}

/**
 * Eu insiro um elemento em uma fila.
 *
 * :param fifo: a fila a se adicionar o elemento
 * :type  fifo: Fifo*
 *
 * :param key: o valor chave do nó
 * :type  key: int
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void enqueue (Fifo* fifo, int key)
{
    // cria um nó
    Node* node = createNode();
    node->key = key;

    // fila é vazia: torna nó criado cabeça
    if (isFifoEmpty(fifo))
    {
        fifo->head = node;
        fifo->tail = node;

        // circularidade da fila
        node->next = node;
        node->previous = node;
        return;
    }

    // insiro o nó ao final da fila circular
    node->next = fifo->head;
    node->previous = fifo->tail;
    fifo->tail->next = node;
    fifo->tail = node;
}

/**
 * Eu libero o espaço de memória de uma fila.
 *
 * :param fifo: a fila a ser liberada
 * :type  fifo: Fifo*
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void freeFifo (Fifo* fifo)
{
    // declaro nó auxiliar
    Node* aux;

    // percorro a fila liberando memória dos nós
    while (fifo->head != fifo->tail);
    {
        aux = fifo->head;
        fifo->head = fifo->head->next;
        free(aux);
    }
    free(fifo->head);

    // libero a memória da própria fila
    free(fifo);
}

/**
 * Eu checo se uma fila é vazia.
 *
 * :param fifo: a fila a ser testada
 * :type  fifo: Fifo*
 *
 * :retorna: SUCCESS ou FAILURE
 * :tipo de retorno: int
 */
int isFifoEmpty (Fifo* fifo)
{
    // fila é nula: retorna SUCCESS
    if (fifo == NULL)
        return SUCCESS;

    // nó cabeça é nulo: retorna SUCCESS
    else if (fifo->head == NULL)
        return SUCCESS;
 
    // retorna FAILURE
    return FAILURE;
}

/**
 * Eu aplico uma prioridade à fila.
 *
 * :param fifo: a fila a ser aplicada prioridade
 * :type  fifo: Fifo*
 *
 * :param key: a chave do elemento a receber prioridade
 * :type  key: int
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void prioritize (Fifo* fifo, int key)
{
    // fila vazia: termina operação
    if (isFifoEmpty(fifo))
        return;

    // pego o nó cabeça e o nó cauda
    Node* head = fifo->head;
    Node* tail = fifo->tail;

    // nó cabeça é o priorizado: termina operação
    if (head->key == key)
        return;

    // nó cauda é o priorizado: faz ele ser o cabeça
    if (tail->key == key)
    {
        fifo->head = fifo->tail;
        fifo->tail->previous = fifo->tail;
        return;
    }

    // encontro o elemento a ser priorizado
    Node* position = head;
    while (position->key != key)
        position = position->next;

    // transformo o elemento priorizado no cabeça
    fifo->head = position;
    position->previous->next = position->next;
    fifo->head->next = head;
    head->previous = fifo->head;
    fifo->head->previous = fifo->tail;
    fifo->tail->next = fifo->head;
}
