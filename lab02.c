/**
 * Nome: Leonardo Victor da Silva Rodrigues
 * RA: 172109
 * Laboratório 02: Matrioshkas Generalizadas
 */

/* Bibliotecas externas */
#include <stdio.h>
#include <stdlib.h>

/* Declaração de constantes */
 #define SUCCESS 1
 #define FAILURE 0

/* Struct for node */
typedef struct Node {
    int key;
    struct Node* previous;
} Node;

/* Struct for stack */
typedef struct Stack {
    int size;
    struct Node* tail;
} Stack;

/* Functions prototypes */
Node* createNode ();
Stack* createStack ();
void destroyNode (Node* node);
void destroyStack (Stack* stack);
void pop (Stack* stack);
void printStack (Stack* stack);
void push (Stack* stack, int key);
Stack* readStack (Stack* stack, int n);
int testStack (Stack* stack);

/**
 * Eu sou a função principal.
 * Eu gerencio o acesso à pilha
 */
int main ()
{
    // local variables
    int size;
    Stack* stack;

    // get initial size
    scanf("%d", &size);
    scanf(" ");

    // read all toys to be tested
    while(size != 0)
    {
        stack = createStack();
        readStack(stack, size);
        if (testStack(stack))
            printf("Eh Matrioshka\n");
        else
            printf("Nao eh Matrioshka\n");
        destroyStack(stack);
        scanf("%d", &size);
    }

    return 0;
}

/**
 * Eu crio um nó.
 *
 * :retorna: o nó criado
 * :tipo de retorno: Node*
 */
Node* createNode ()
{
    return (Node *) calloc (1, sizeof(Node));
}

/**
 * Eu crio uma pilha.
 *
 * :retorna: a pilha criada
 * :tipo de retorno: Stack*
 */
Stack* createStack ()
{
    return (Stack *) calloc (1, sizeof(Stack));
}

/**
 * Eu destruo uma pilha.
 *
 * :param stack: a pilha a ser destruída
 * :type  stack: Stack*
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void destroyStack (Stack* stack)
{
    // get stack tail
    Node* position = stack->tail;
    Node* aux = stack->tail->previous;

    while (aux != NULL)
    {
        free(position);
        position = aux;
        aux = aux->previous;
    }

    free(stack);
}

/**
 * Eu removo o nó do fim da pilha.
 *
 * :param stack: a pilha a ter o nó removido
 * :type  stack: Stack*
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void pop (Stack* stack)
{
    // get stack tail
    Node* tail = stack->tail;

    // stack is empty: finish
    if (stack->size == 0)
        return;

    // make previous the tail
    stack->tail = tail->previous;

    // delete former tail
    free(tail);

    // decrease stack size
    stack->size--;
}

/**
 * Eu imprimo uma pilha.
 *
 * :param stack: a pilha a ser impressa
 * :type  stack: Stack*
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void printStack (Stack* stack)
{
    // get stack tail
    Node* position = stack->tail;

    // iterate by stack printing its nodes keys
    while (position != NULL)
    {
        printf(" <-- %d", position->key);
        position = position->previous;
    }
    printf("\n");
}

/**
 * Eu insiro um nó ao fim da pilha.
 *
 * :param stack: a pilha a ter um nó inserido
 * :type  stack: Stack*
 *
 * :param key: o valor a ser inserido no nó
 * :type  key: int
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void push (Stack* stack, int key)
{
    // get stack tail
    Node* tail = stack->tail;

    // create node
    Node* node = createNode();
    node->key = key;

    // stack is not empty: insert it
    if (stack->size != 0)
        node->previous = tail;
    
    // make new node as tail
    stack->tail = node;

    // increase stack size
    stack->size++;
}

/**
 * Eu leio a totalidade de uma pilha de tamanho ´n´.
 *
 * :param stack: a pilha a ser lida
 * :type  stack: Stack*
 *
 * :param n: o tamanho da pilha a ser lida
 * :type  n: int
 *
 * :retorna: a pilha lida
 * :tipo de retorno: Stack*
 */
Stack* readStack (Stack* stack, int n)
{
    // declare local variables
    int key, i;

    // read stack elements
    for (i = 0; i < n; i++)
    {
        scanf("%d", &key);
        push(stack, key);
    }
    scanf(" ");

    return stack;
}

/**
 * Eu testo se uma pilha representa uma Matrioshka.
 *
 * :param node:
 * :type  node: Node*
 *
 * :param position:
 * :type  position: Node*
 *
 * :retorna: SUCCESS ou FAILURE
 * :tipo de retorno: int
 */
int testStack (Stack* stack)
{
    // get tail of stack
    Node* position = stack->tail;

    // create auxiliar stack
    Stack* aux = createStack();

    int size = 0;

    // iterate through stack
    while (position != NULL)
    {
        printf(" <-- %d ", position->key);
        /* // new matrioshka smaller than a current one: open it
        if (position->key > 0 && position->key < aux->tail->key)
        {
            push(aux, position->key);
        }

        // matrioshka being closed: close it
        else if (abs(aux->tail->key) == abs(position->key))
        {
            size += abs(position->key);
            pop(aux);
        }

        // any thing wrong: return FAILURE
        else
        {
            destroyStack(aux);
            return FAILURE;
        } */ 

        // go backward
        position = position->previous;
    }

    // any matrioshka left: return FAILURE
    if (aux->tail != NULL)
    {
        destroyStack(aux);
        return FAILURE;
    }

    // all matrioshka close: return SUCCESS
    else
    {
        destroyStack(aux);
        return SUCCESS;
    }
}
