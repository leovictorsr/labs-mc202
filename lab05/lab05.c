/**
 * Nome: Leonardo Victor da Silva Rodrigues
 * RA: 172109
 * Laboratório 05 - A calculadora pseudo-científica
 */

/* Bibliotecas */
#include <stdio.h>
#include <stdlib.h>

/* Constantes */

/* Structs */
typedef struct Tree {
    int data;
    struct Tree* left;
    struct Tree* right;
} Tree;

/* Protótipo de funções */
int calculate(Tree* tree);
void chopTree (Tree* tree);
Tree* createTree();
Tree* readTree();


/* Eu sou a função principal e gerencio a árvore.
 *
 * :retorna: 0
 * :tipo de retorno: int
 */
int main ()
{
    /* Declaração de variáveis locais */
    int result;

    /* Leio a expressão no formato de árvore */
    Tree* tree = readTree();

    /* Calculo a expressão da árvore */
    result = calculate(tree);

    /* Imprimo o resultado */
    printf("%d\n", result);

    /* Corto a árvore */
    chopTree(tree);

    /* Fim */
    return 0;
}


/**
 * Eu calculo uma expressão no formato de uma árvore.
 *
 * :param tree: a árvore a ser calculada
 * :type  tree: Tree*
 *
 * :retorna: o resultado da expressão
 * :tipo de retorno: int
 */
int calculate (Tree* tree)
{
    /* Declaração de variáveis locais */
    int result = 0;

    /* Árvore é nula: retorno 0 */
    if (tree == NULL)
    {
        return 0;
    }

    /* Árvore tem filhos esquerdo e direito: calculo o resultado deles */
    if (tree->left != NULL && tree->right != NULL)
    {
        /* Se o valor do nó... */
        switch (tree->data)
        {
            /* ... é -1: calculo a soma das árvores filhas */
            case -1:
                result = calculate(tree->left) + calculate(tree->right);
                break;

            /* ... é -2: calculo a subtração das árvores filhas */
            case -2:
                result = calculate(tree->left) - calculate(tree->right);
                break;

            /* ... é -3: calculo a multiplicação das árvores filhas */
            case -3:
                result = calculate(tree->left) * calculate(tree->right);
                break;

            /* ... é -4: calculo a divisão das árvores filhas */
            case -4:
                result = calculate(tree->left) / calculate(tree->right);
                break;
        }
    }

    /* Nó é folha: o resultado é o valor do nó */
    else
    {
        result = tree->data;
    }

    /* Retorno o resultado */
    return result;
}


/**
 * Eu corto uma árvore.
 *
 * :param tree: a árvore a ser cortada
 * :type  tree: Tree*
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void chopTree (Tree* tree)
{
    /* Árvore possui filhas: corto elas também */
    if (tree->left != NULL && tree->right != NULL)
    {
        chopTree(tree->left);
        chopTree(tree->right);
    }

    /* Corto a árvore */
    free(tree);
}


/**
 * Eu crio uma árvore.
 *
 * :retorna: a árvore criada
 * :tipo de retorno: Tree*
 */
Tree* createTree ()
{
    return (Tree*) calloc (1, sizeof(Tree));
}


/**
 * Eu leio uma expressão e a traduzo no formato de árvore.
 *
 * :retorna: o nó raiz da árvore criada com a expressão
 * :tipo de retorno: Tree*
 */
Tree* readTree ()
{
    /* Declaração de variáveis locais */
    int iAux = 0;
    char aux;

    /* Leio o caractere */
    scanf("%c", &aux);

    /* Caractere ')' ou '\n': a árvore é nula */
    if (aux == ')' || aux == '\n')
    {
        return NULL;
    }

    /* Outro caractere lido: continua lendo a árvore */
    else
    {
        /* Crio a árvore */
        Tree* tree =  createTree();

        /* Caractere lido é '(': nova árvore esquerda */
        if (aux == '(')
        {
            tree->left = readTree();
        }

        /* Leio o próximo caractere */
        scanf("%c", &aux);

        /* Fim da linha: retorna a árvore */
        if (aux == '\n')
        {
            return tree;
        }

        /* Sinal negativo: lê o número seguinte como negativo */
        else if (aux == '-')
        {
            scanf("%d", &iAux);
            iAux = -1 * iAux;

            /* Leio o próximo caractere */
            scanf("%c", &aux);
        }

        /* Qualquer número: lê seus dígitos até achar uma nova árvore */
        else
        {
            /* Enquanto não houver uma nova árvore... */
            while (aux != '(')
            {
                /* Aumento uma casa decimal do número */
                iAux = iAux * 10;

                /* Parseio o char em int e somo ao número */
                iAux = iAux + ((int) aux - '0');

                /* Leio o próximo caractere */
                scanf("%c", &aux);
            }
        }

        /* Atribuo o número encontrado ao nó */
        tree->data = iAux;

        /* Caractere lido é '(': nova árvore direita */
        if (aux == '(')
        {
            tree->right = readTree();
        }

        /* Escaneio o símbolo ')' do fim da árvore direita */
        scanf("%c", &aux);

        /* Retorno o nó raiz da árvore criada */
        return tree;
    }
}
