#include "ArvoreBinaria.h"
#include "ArvoreBinaria.c"

int main(int argc, char **argv)
{
  ArvoreBinaria *T=NULL;
  char *s=NULL, *percurso;
  int   n;

  if (argc != 3) {
    printf("ConstroiArvoreBinaria <ordem-simetrica> <pos-ordem>\n");
    printf("ordem-simetrica: string com os nos em ordem simetrica\n");
    printf("pos-ordem: string com os nos em pos ordem\n");
    exit(1);
  }

  if (strlen(argv[1])!=strlen(argv[2])){
    printf("Percursos dados nao tem o mesmo tamanho\n");
    exit(1);
  }

  n = strlen(argv[1]);
  T = ConstroiArvoreBinariaSimPos(argv[1], argv[2], n); 

  ImprimeArvoreBinaria(T,0,Altura(T));
    
  s = (char *)calloc(n,sizeof(char));
  percurso = s;
  printf("Percurso em Pre-Ordem\n");
  VisitaPreOrdem(T,&s); /* s retorna apontando para o final da string */  
  printf("%s\n",percurso);

  s= percurso;
  printf("Percurso em Ordem Simetrica\n");
  VisitaOrdemSimetrica(T,&s); /* s retorna apontando para o final da string */  
  printf("%s\n",percurso);

  s= percurso;
  printf("Percurso em Pos-Ordem\n");
  VisitaPosOrdem(T,&s); /* s retorna apontando para o final da string */  
  printf("%s\n",percurso);

  free(percurso);
  DestroiArvoreBinaria(&T);
 
  return(0);
}
