/**
 * Nome: Leonardo Victor da Silva Rodrigues
 * RA: 172109
 * Laboratório 06 - Queria o MySQL
 */

//
// Constants
//


//
// Headers
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//
// Structs
//

// Record
typedef struct Record {
    unsigned int id;
    char data[1000];
} Record;

// AVL Tree
typedef struct Tree {
    unsigned int address;
    int balance;
    unsigned int id;
    unsigned int height;
    struct Tree* left;
    unsigned int leftHeight;
    struct Tree* right;
    unsigned int rightHeight;
} Tree;


//
// Function prototypes
//
Record* createRecord ();
Tree* createTree ();
int insert (Tree* tree, Record* record, int address);
Tree* readRecords (const char* filename);
void rotateLeft (Tree* tree);
void rotateRight (Tree* tree);
void rotateDoubleLeft (Tree* tree);
void rotateDoubleRight (Tree* tree);


/**
 * I am the main function.
 * I manage the workflow for reading and executing commands.
 *
 * :returns: 0
 * :return type: int
 */
int main ()
{
    // declare local variables
    char filename[20];
    Tree* records;

    // read file name
    scanf("%s", filename);

    // read record inside file
    records = readRecords(filename);

    // end of script
    return 0;
}


/**
 * I balance a tree.
 *
 * :parameter tree: tree to be balanced
 * :parameter type: Tree*
 *
 * :returns: nothing
 * :return type: void
 */
int balance(Tree* tree)
{
    // calculate tree balance
    tree->balance = tree->rightHeight - tree->leftHeight;

    // tree is balanced: nothing to do
    if (balance >= -1 && balance <= 1)
    {
        return tree->balance;
    }

    // right tree is unbalanced: rotate left
    else if (tree->balance > 1)
    {
        // right tree is already left heavy: rotate tree double left
        if (balance(tree->right) < 0)
        {
            rotateDoubleLeft(tree);
        }

        // right tree is fine: rotate tree single left
        else
        {
            rotateLeft(tree);
        }

        // return tree balance
        return balance(tree->balance);
    }

    // left tree is unbalanced: rotate right
    else
    {
        // left tree is already right heavy: rotate tree double right
        if (balance(tree->left) > 0)
        {
            rotateDoubleRight(tree);
        }

        // left tree is fine: rotate tree single right
        else
        {
            rotateRight(tree);
        }

        // return tree balance
        return balance(tree->balance);
    }
}


/**
 * I create a record.
 *
 * :returns: created reacord
 * :return type: Record*
 */
Record* createRecord ()
{
    return (Record *) calloc (1, sizeof(Record));
}


/**
 * I create a tree.
 *
 * :returns: created tree
 * :return type: Tree*
 */
Tree* createTree ()
{
    return (Tree *) calloc (1, sizeof(Tree));
}


/**
 * I insert a read record into an AVL tree.
 *
 * :parameter tree: where the record will inserted into
 * :parameter type : Tree*
 *
 * :parameter record: source of data to be inserted
 * :parameter type: Record*
 *
 * :parameter address: record address inside file
 * :parameter type: int
 *
 * :returns: height of the tree
 * :return type: int
 */
int insert (Tree* tree, Record* record, int address)
{
    /* Empty tree: create the tree */
    if (tree == NULL)
    {
        /* Create tree */
        tree = createTree();

        /* Store record data */
        tree->id = record->id;
        tree->address = address;
        tree->height = 1;

        /* Return tree height */
        return tree->height;
    }

    /* Smaller id: insert to the left */
    else if (record->id < tree->id)
    {
        tree->leftHeight = insert(tree->left, record, address);
    }

    /* Bigger id: insert to the right */
    else if (record->id > tree->id)
    {
        tree->rightHeight = insert(tree->right, record, address);
    }

    // left tree is higher: update height
    if (tree->leftHeight > tree->rightHeight)
    {
        tree->height = tree->leftHeight + 1;
    }

    // right tree is higher: update height
    else
    {
        tree->height = tree->rightHeight + 1;
    }

    // balance tree
    balance(tree);

    // return tree height
    return tree->height;
};


/**
 * I read the records from a binary file and store them in a AVL tree.
 *
 * :parameter filename: name of the file to be open
 * :parameter type: String
 *
 * :returns: AVL tree that store the records
 * :return type: Tree*
 */
Tree* readRecords (const char *filename)
{
    /* Declaration of local variables */
    FILE *records = NULL;
    Record *record = createRecord();
    Tree *tree;

    /* Open file */
    records = fopen(filename, "rb");

    /* File error: alert and finish */
    if (records == NULL)
    {
        printf("Erro ao abrir o arquivo!\n");
        return NULL;
    }

    /* Read records and insert them into tree */
    while (fread(record, sizeof(Record), 1, records))
    {
        insert(tree, record, ftell(records));
    }
}


/**
 * I rotate the tree to the left.
 *
 * :parameter tree: the tree to be rotated
 * :parameter type: Tree*
 *
 * :returns: nothing
 * :return type: void
 */
void rotateLeft (Tree* tree)
{

}


/**
 * I rotate the tree to the right.
 *
 * :parameter tree: the tree to be rotated
 * :parameter type: Tree*
 *
 * :returns: nothing
 * :return type: void
 */
void rotateRight (Tree* tree)
{

}


/**
 * I double rotate the tree to the left, which is a rotation to the right and 
 * other to the left.
 *
 * :parameter tree: the tree to be rotated
 * :parameter type: Tree*
 *
 * :returns: nothing
 * :return type: void
 */
void rotateDoubleLeft (Tree* tree)
{
    // rotate right
    rotateRight(tree);

    // rotate left
    rotateLeft(tree);
}


/**
 * I double rotate the tree to the left, which is a rotation to the left and 
 * other to the right.
 *
 * :parameter tree: the tree to be rotated
 * :parameter type: Tree*
 *
 * :returns: nothing
 * :return type: void
 */
void rotateDoubleRight (Tree* tree)
{
    // rotate left
    rotateLeft(tree);

    // rotate right
    rotateRight(tree);
}
